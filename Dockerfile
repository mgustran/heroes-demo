FROM adoptopenjdk/openjdk11:alpine

RUN mkdir /logs
RUN mkdir /db
RUN addgroup -S spring && adduser -S spring -G spring
RUN chown -R spring:spring /logs
RUN chown -R spring:spring /db
USER spring:spring
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","app.jar"]