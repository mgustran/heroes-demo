INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (1, 'Spiderman', 'male', 188, 'Peter', 'human', 'Parker', 85.5);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (2, 'Batman', 'male', 158, 'Bruce', 'human', 'Wayne', 82.5);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (3, 'V', 'male', 170, 'V', 'human', '', 89.5);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (4, 'Wolverine', 'male', 160, 'James', 'mutant', 'Howlett', 136.1);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (5, 'Superman', 'male', 190.5, 'Clark Joseph', 'kryptonian', 'Kent (Kal-El)', 106.6);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (6, 'Hulk', 'male', 243.8, 'Robert Bruce', 'human/radiation', 'Banner', 635);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (7, 'Wonder Woman', 'female', 188, 'Diana', 'amazon', 'Of Themyscira', 74.8);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (8, 'Thor', 'male', 198.1, 'Thor', 'asgardian', 'Odinson', 290.3);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (9, 'Professor X', 'male', 170.2, 'Charles Francis', 'mutant', 'Xavier', 67.1);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (10, 'Neo', 'male', 182.9, 'Thomas A.', 'human', 'Anderson', 83.2);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (11, 'Black Widow', 'female', 170, 'Natalia', 'human', 'Alianovna Romanova', 59);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (12, 'Batgirl', 'female', 170.1, 'Barbara', 'human', 'Gordon', 61);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (13, 'Storm', 'female', 181.3, 'Ororo', 'mutant', 'Iqadi Munroe', 66);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (14, 'Hellboy', 'male', 210.2, 'Anung', 'demon', 'Un Rama', 180.4);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (15, 'Deadpool', 'male', 188.9, 'Wade', 'mutant', 'Winston Wilson', 81.5);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (16, 'Phoenix', 'female', 169.4, 'Jean Elaine', 'mutant', 'Grey Summers', 52);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (17, 'Invisible Woman', 'female', 168.8, 'Susan "Sue"', 'human/radiation', 'Storm Richards', 64.7);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (18, 'Catwoman', 'female', 174.4, 'Selina', 'human', 'Kyle', 59.4);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (19, 'Daredevil', 'male', 180.3, 'Matthew Michael', 'human/radiation', 'Murdock', 91.2);
INSERT INTO HERO (ID, ALIAS, GENDER, HEIGHT_CM, NAME, SPECIES, SURNAMES, WEIGHT_KG) VALUES (20, 'The Punisher', 'male', 194.3, 'Frank', 'human', 'Castle', 84.7);

INSERT INTO USERS (ID, EMAIL, NAME, PASSWORD, USERNAME) VALUES (1, 'mgustran@gmail.com', 'Mike Lowrey', '$2a$10$Yi2Bz1/1EjhvfOwonsSxMePXuC9L0R9exeTXzWGKYmhicmAJ.ixM.', 'mgustran');

INSERT INTO ROLES(ID, NAME) VALUES(1, 'ROLE_USER');
INSERT INTO ROLES(ID, NAME) VALUES(2, 'ROLE_PM');
INSERT INTO ROLES(ID, NAME) VALUES(3, 'ROLE_ADMIN');

INSERT INTO USER_ROLES (USER_ID, ROLE_ID) VALUES (1, 3);