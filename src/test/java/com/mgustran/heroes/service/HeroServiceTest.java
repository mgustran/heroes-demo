package com.mgustran.heroes.service;

import com.mgustran.heroes.exception.HeroNotFoundException;
import com.mgustran.heroes.model.Hero;
import com.mgustran.heroes.repository.HeroRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HeroServiceTest {

    private static final String HERO_KEY_TO_FIND = "man";
    private static final String HERO_NAME_TO_MODIFY = "Peterrrr";
    private static final List<Hero> HERO_MOCK_LIST = getHeroListMock();

    private HeroService service;

    @Mock
    private HeroRepository repository;

    @BeforeEach
    public void before() {
        service = new HeroServiceImpl(repository);
    }

    @Test
    @DisplayName("return all instances from hero in repository")
    public void test1() {
        when(repository.findAll()).thenReturn(HERO_MOCK_LIST);

        assertDoesNotThrow(() -> {
            List<Hero> result = service.getAllHeroes();
            assertEquals(result.size(), 3);
            assertEquals(result.get(0).getId(), 1);
            assertEquals(result.get(2).getId(), 3);
        });

        verify(repository).findAll();
    }

    @Test
    @DisplayName("return hero by id")
    public void test2() {
        Optional<Hero> hero = Optional.of(HERO_MOCK_LIST.get(0));

        when(repository.findById(1L)).thenReturn(hero);

        assertDoesNotThrow(() -> {
            Hero result = service.getHeroById(1L);
            assertNotNull(result);
            assertEquals(result.getId(), 1L);
        });

        verify(repository).findById(1L);
    }

    @Test
    @DisplayName("return all instances from hero containing string in alias")
    public void test3() {
        List<Hero> heroList = HERO_MOCK_LIST
                .stream().filter(hero -> hero.getAlias().contains(HERO_KEY_TO_FIND)).collect(Collectors.toList());

        when(repository.findHeroesByAliasContains(HERO_KEY_TO_FIND)).thenReturn(heroList);

        assertDoesNotThrow(() -> {
            List<Hero> result = service.getHeroesByAliasContains(HERO_KEY_TO_FIND);
            assertNotNull(result);
            assertTrue(result.stream().allMatch(hero -> hero.getAlias().contains(HERO_KEY_TO_FIND)));
        });

        verify(repository).findHeroesByAliasContains(HERO_KEY_TO_FIND);
    }

    @Test
    @DisplayName("modify hero (by id)")
    public void test4() {
        Optional<Hero> hero = Optional.of(HERO_MOCK_LIST.get(0));

        when(repository.findById(1L)).thenReturn(hero);
        assertDoesNotThrow(() -> {
            Hero result = service.getHeroById(1L);
            assertNotNull(result);
            assertEquals(result.getId(), 1L);

            result.setName(HERO_NAME_TO_MODIFY);
            service.modifyHero(result);

        });

        Hero modifiedHero = hero.get();
        modifiedHero.setName(HERO_NAME_TO_MODIFY);
        when(repository.findById(1L)).thenReturn(Optional.of(modifiedHero));

        assertDoesNotThrow(() -> {
            Hero result = service.getHeroById(1L);
            assertNotNull(result);
            assertEquals(result.getName(), HERO_NAME_TO_MODIFY);
        });


        verify(repository, times(3)).findById(1L);
        verify(repository, times(1)).save(modifiedHero);
    }

    @Test
    @DisplayName("remove hero by id")
    public void test5() {
        Optional<Hero> hero = Optional.of(HERO_MOCK_LIST.get(0));

        when(repository.findById(1L)).thenReturn(hero);
        assertDoesNotThrow(() -> {
            Hero result = service.getHeroById(1L);
            assertNotNull(result);
            assertEquals(result.getId(), 1L);

            service.removeHero(1L);
        });

        when(repository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(HeroNotFoundException.class, () -> {
            Hero result = service.getHeroById(1L);
        });

        verify(repository, times(3)).findById(1L);
        verify(repository, times(1)).deleteById(1L);
    }


    private static List<Hero> getHeroListMock() {

        return new ArrayList<>(Arrays.asList(
                Hero.builder()
                        .id(1L).name("Peter").surnames("Parker").alias("Spiderman")
                        .gender("male").species("human").heightCm(188F).weightKg(85.5F)
                        .build(),
                Hero.builder()
                        .id(2L).name("Bruce").surnames("Wayne").alias("Batman")
                        .gender("male").species("human").heightCm(158F).weightKg(82.5F)
                        .build(),
                Hero.builder()
                        .id(3L).name("V").surnames("").alias("V")
                        .gender("male").species("human").heightCm(170F).weightKg(89.5F)
                        .build()));
    }
}
