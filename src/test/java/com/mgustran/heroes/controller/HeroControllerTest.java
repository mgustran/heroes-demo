package com.mgustran.heroes.controller;

import com.mgustran.heroes.HeroesApplication;
import com.mgustran.heroes.exception.HeroNotFoundException;
import com.mgustran.heroes.model.Hero;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@WithMockUser(username = "mgustran", password = "test-password", roles = "ADMIN")
@SpringBootTest(classes = HeroesApplication.class)
public class HeroControllerTest {

    private static final Hero MODIFIED_HERO = Hero.builder()
            .id(1L).name("Peter Benjamin").surnames("Parker").alias("Spiderman")
            .gender("male").species("human").heightCm(188F).weightKg(87.5F)
            .build();

    @Autowired
    HeroController controller;

    @Test
    @DisplayName("return all heroes")
    void test1() {
        assertDoesNotThrow(() -> {
            ResponseEntity<List<Hero>> result = controller.getHeroes(null);
            assertEquals(HttpStatus.OK, result.getStatusCode());
            assertNotNull(result.getBody());
        });
    }

    @Test
    @DisplayName("return hero by id")
    void test2() {
        assertDoesNotThrow(() -> {
            ResponseEntity<Hero> result = controller.getHeroById(1L);
            assertEquals(HttpStatus.OK, result.getStatusCode());
            assertNotNull(result.getBody());
        });
    }

    @Test
    @DisplayName("return heroes that contains key in alias")
    void test3() {
        assertDoesNotThrow(() -> {
            ResponseEntity<List<Hero>> result = controller.getHeroes("man");
            assertEquals(HttpStatus.OK, result.getStatusCode());
            assertNotNull(result.getBody());
        });
    }

    @Test
    @DisplayName("modify hero by id")
    void test4() {
        assertDoesNotThrow(() -> {
            ResponseEntity<Void> result = controller.modifyHero(1L, MODIFIED_HERO);
            assertEquals(HttpStatus.OK, result.getStatusCode());

            ResponseEntity<Hero> result2 = controller.getHeroById(1L);
            assertNotNull(result2.getBody());
            assertEquals(result2.getBody().getName(), MODIFIED_HERO.getName());
        });
    }

    @Test
    @DisplayName("remove hero by id")
    void test5() {
        assertDoesNotThrow(() -> {
            ResponseEntity<Void> result = controller.removeHero(1L);
            assertEquals(HttpStatus.OK, result.getStatusCode());
        });

        assertThrows(HeroNotFoundException.class, () -> {
            ResponseEntity<Hero> result = controller.getHeroById(1L);
        });
    }
}
