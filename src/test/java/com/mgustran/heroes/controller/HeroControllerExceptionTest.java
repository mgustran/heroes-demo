package com.mgustran.heroes.controller;

import com.mgustran.heroes.HeroesApplication;
import com.mgustran.heroes.exception.HeroNotFoundException;
import com.mgustran.heroes.model.Hero;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = HeroesApplication.class)
@WithMockUser(username = "mgustran", password = "test-password", roles = "ADMIN")
public class HeroControllerExceptionTest {

    @Autowired
    HeroController controller;

    @Test
    @DisplayName("throw HeroNotFoundException")
    void test1() {
        assertThrows(HeroNotFoundException.class, () -> {
            ResponseEntity<Hero> result = controller.getHeroById(666L);
            assertEquals(HttpStatus.OK, result.getStatusCode());
            assertNotNull(result.getBody());
        });
    }

}
