package com.mgustran.heroes;

import com.mgustran.heroes.exception.HeroNotFoundException;
import com.mgustran.heroes.exception.RestError;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ControllerAdvice
public class RestCustomExceptionHandler {

    @ExceptionHandler({ ConstraintViolationException.class })
    public ResponseEntity<Object> handleConstraintViolation(
            ConstraintViolationException ex, WebRequest request) {
        List<String> errors = new ArrayList<>();
        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            errors.add(violation.getRootBeanClass().getName() + " " +
                    violation.getPropertyPath() + ": " + violation.getMessage());
        }

        RestError restError = new RestError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
        return new ResponseEntity<>(restError, new HttpHeaders(), restError.getStatus());
    }

    @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
            MethodArgumentTypeMismatchException ex, WebRequest request) {
        String error = ex.getName() + " should be of type " + ex.getRequiredType().getName();

        RestError restError =
                new RestError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), Collections.singletonList(error));
        return new ResponseEntity<>(
                restError, new HttpHeaders(), restError.getStatus());
    }

    @ExceptionHandler({ TypeMismatchException.class })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
            TypeMismatchException ex, WebRequest request) {
        String error = ex.getPropertyName() + " should be of type " + ex.getRequiredType().getName();

        RestError restError =
                new RestError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), Collections.singletonList(error));
        return new ResponseEntity<>(
                restError, new HttpHeaders(), restError.getStatus());
    }

    @ExceptionHandler({ AccessDeniedException.class })
    public ResponseEntity<Object> handleAccessDenied(AccessDeniedException ex, WebRequest request) {
        RestError restError = new RestError(
                HttpStatus.FORBIDDEN,
                ex.getLocalizedMessage(),
                Collections.singletonList("You dont have the role or permission to access the resource"));
        return new ResponseEntity<>(restError, new HttpHeaders(), restError.getStatus());
    }

    @ExceptionHandler({ HeroNotFoundException.class })
    public ResponseEntity<Object> handleHeroNotFound(HeroNotFoundException ex, WebRequest request) {
        RestError restError = new RestError(
                HttpStatus.BAD_REQUEST,
                ex.getLocalizedMessage(),
                Collections.singletonList("error occurred"));
        return new ResponseEntity<>(restError, new HttpHeaders(), restError.getStatus());
    }

    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
        RestError restError = new RestError(
                HttpStatus.INTERNAL_SERVER_ERROR,
                ex.getLocalizedMessage(),
                Collections.singletonList("error occurred"));
        return new ResponseEntity<>(restError, new HttpHeaders(), restError.getStatus());
    }
}