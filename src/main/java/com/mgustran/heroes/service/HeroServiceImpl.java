package com.mgustran.heroes.service;

import com.mgustran.heroes.exception.HeroNotFoundException;
import com.mgustran.heroes.model.Hero;
import com.mgustran.heroes.repository.HeroRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class HeroServiceImpl implements HeroService {

    @Autowired
    private HeroRepository repository;

    public Hero getHeroById(Long id) throws HeroNotFoundException {
        Optional<Hero> result = repository.findById(id);
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new HeroNotFoundException("Hero with id " + id + " not found");
        }
    }

    public List<Hero> getAllHeroes() {
        return repository.findAll();
    }

    public List<Hero> getHeroesByAliasContains(String search) {
        return repository.findHeroesByAliasContains(search);
    }

    public void modifyHero(Hero hero) throws HeroNotFoundException {
        if (repository.findById(hero.getId()).isPresent()) {
            repository.save(hero);
        } else {
            throw new HeroNotFoundException("Hero with id " + hero.getId() + " not found");
        }
    }

    public void removeHero(Long id) throws HeroNotFoundException {
        if (repository.findById(id).isPresent()) {
            repository.deleteById(id);
        } else {
            throw new HeroNotFoundException("Hero with id " + id + " not found");
        }
    }
}
