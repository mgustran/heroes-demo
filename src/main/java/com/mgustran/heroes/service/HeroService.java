package com.mgustran.heroes.service;

import com.mgustran.heroes.exception.HeroNotFoundException;
import com.mgustran.heroes.model.Hero;

import java.util.List;

public interface HeroService {

    Hero getHeroById(Long id) throws HeroNotFoundException;
    List<Hero> getAllHeroes();
    List<Hero> getHeroesByAliasContains(String search);

    void modifyHero(Hero hero) throws HeroNotFoundException;
    void removeHero(Long id) throws HeroNotFoundException;
}
