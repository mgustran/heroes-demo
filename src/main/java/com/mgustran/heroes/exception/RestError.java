package com.mgustran.heroes.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.List;

@Getter
@AllArgsConstructor
public class RestError {

    private final HttpStatus status;
    private final String message;
    private final List<String> errors;
}
