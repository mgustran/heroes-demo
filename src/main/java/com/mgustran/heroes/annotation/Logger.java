package com.mgustran.heroes.annotation;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class Logger
{
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Logger.class);
    @Around("execution(* *(..)) && @annotation(com.mgustran.heroes.annotation.Metrics)")
    public Object log(ProceedingJoinPoint point) throws Throwable {
        long start = System.currentTimeMillis();
        Object result = point.proceed();
        logger.info("class={},   method={},   timeMs={},    threadId={}",
                point.getSignature().getDeclaringTypeName(),
                ((MethodSignature) point.getSignature()).getMethod().getName(),
                System.currentTimeMillis() - start,
                Thread.currentThread().getId());
        return result;
    }
}