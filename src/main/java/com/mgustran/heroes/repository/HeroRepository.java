package com.mgustran.heroes.repository;

import com.mgustran.heroes.model.Hero;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface HeroRepository extends CrudRepository<Hero, Long> {

    @Override
    List<Hero> findAll();

    List<Hero> findHeroesByAliasContains(String key);
}
