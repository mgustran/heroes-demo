package com.mgustran.heroes.controller;


import io.swagger.v3.oas.annotations.security.SecurityRequirement;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;



@RestController
@RequestMapping("/api")
@SecurityRequirement(name = "bearerAuth")
public class LogController {

    private static final String EXTERNAL_FILE_PATH = "logs/metrics-logger.log";

    @GetMapping("/log/metrics")
    @PreAuthorize("hasRole('ADMIN')")
    public void downloadMetricsLog(HttpServletRequest request, HttpServletResponse response) throws IOException {

        File file = new File(EXTERNAL_FILE_PATH);
        if (file.exists()) {

            String mimeType = URLConnection.guessContentTypeFromName(file.getName());
            if (mimeType == null) {
                mimeType = "application/octet-stream";
            }

            response.setContentType(mimeType);
            response.setHeader("Content-Disposition", "inline; filename=\"" + file.getName() + "\"");
            //response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));

            response.setContentLength((int) file.length());

            InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

            FileCopyUtils.copy(inputStream, response.getOutputStream());

        }
    }


}
