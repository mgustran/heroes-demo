package com.mgustran.heroes.controller;

import com.mgustran.heroes.annotation.Metrics;
import com.mgustran.heroes.exception.HeroNotFoundException;
import com.mgustran.heroes.model.Hero;
import com.mgustran.heroes.service.HeroService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RestController
@RequestMapping("/api")
@SecurityRequirement(name = "bearerAuth")
public class HeroController {

    @Autowired
    private HeroService service;

    @GetMapping(value = "/heroes/{id}", produces = APPLICATION_JSON_VALUE)
    @Metrics
    @Cacheable(value = "hero", key = "#id")
    @PreAuthorize("hasRole('USER') or hasRole('PM') or hasRole('ADMIN')")
    public ResponseEntity<Hero> getHeroById(@PathVariable Long id) throws HeroNotFoundException {
        return ResponseEntity.ok(service.getHeroById(id));
    }

    @GetMapping(value = "/heroes", produces = APPLICATION_JSON_VALUE)
    @Metrics
    @Cacheable(value = "heroes")
    @PreAuthorize("hasRole('USER') or hasRole('PM') or hasRole('ADMIN')")
    public ResponseEntity<List<Hero>> getHeroes(@RequestParam(name = "key", required = false) String key) {
        if (key == null) {
            return ResponseEntity.ok(service.getAllHeroes());
        } else if (key.isBlank()) {
            return ResponseEntity.badRequest().build();
        } else {
            return ResponseEntity.ok(service.getHeroesByAliasContains(key));
        }
    }

    @PutMapping(value = "/heroes/{id}", consumes = APPLICATION_JSON_VALUE)
    @Metrics
    @Caching(evict = {
            @CacheEvict(value = "heroes", allEntries = true),
            @CacheEvict(value = "hero", key = "#id")})
    @PreAuthorize("hasRole('PM') or hasRole('ADMIN')")
    public ResponseEntity<Void> modifyHero(@PathVariable Long id, @Valid @RequestBody Hero hero) throws HeroNotFoundException {
        if (hero.getId() != null && !hero.getId().equals(id)) {
            throw new HeroNotFoundException("Hero object contains different id than path param, sync field or remove id from body");
        } else {
            hero.setId(id);
        }
        service.modifyHero(hero);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/heroes/{id}")
    @Metrics
    @Caching(evict = {
            @CacheEvict(value = "heroes", allEntries = true),
            @CacheEvict(value = "hero", key = "#id")})
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Void> removeHero(@PathVariable Long id) throws HeroNotFoundException {
        service.removeHero(id);
        return ResponseEntity.ok().build();
    }



}
