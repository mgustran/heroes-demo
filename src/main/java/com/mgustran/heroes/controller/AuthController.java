package com.mgustran.heroes.controller;



import com.mgustran.heroes.exception.RestError;
import com.mgustran.heroes.message.request.Login;
import com.mgustran.heroes.message.request.Register;
import com.mgustran.heroes.message.response.JwtResponse;
import com.mgustran.heroes.model.Role;
import com.mgustran.heroes.model.RoleName;
import com.mgustran.heroes.model.User;
import com.mgustran.heroes.repository.RoleRepository;
import com.mgustran.heroes.repository.UserRepository;
import com.mgustran.heroes.security.jwt.JwtProvider;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtProvider jwtProvider;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody Login loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtProvider.generateJwtToken(authentication);
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();

		return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
	}

	@PostMapping("/signup")
	@PreAuthorize("hasRole('PM') or hasRole('ADMIN')")
	@Operation(security = @SecurityRequirement(name = "bearerAuth"))
	public ResponseEntity<?> registerUser(@Valid @RequestBody Register signUpRequest) {

		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<>(
					new RestError(BAD_REQUEST, "Username is already in use", Collections.emptyList()),
					BAD_REQUEST);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<>(
					new RestError(BAD_REQUEST, "Email is already in use", Collections.emptyList()),
					BAD_REQUEST);
		}

		User user = new User(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
				encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();


		for (String role : strRoles) {
			switch (role) {
				case "admin":
					roleRepository.findByName(RoleName.ROLE_ADMIN).ifPresent(roles::add);
					break;
				case "pm":
					roleRepository.findByName(RoleName.ROLE_PM).ifPresent(roles::add);
					break;
				default:
					roleRepository.findByName(RoleName.ROLE_USER).ifPresent(roles::add);
			}
		}

		user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok().build();
	}
}