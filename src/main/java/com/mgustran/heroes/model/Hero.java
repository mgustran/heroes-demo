package com.mgustran.heroes.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Hero {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Name is required")
    @Size(min=1, max = 20, message = "Name length must be between 1-20")
    @Column(unique = true, nullable = false)
    private String name;

    @Size(max = 40, message = "Surnames length must be between 0-40")
    @Column(nullable = false)
    private String surnames;

    @NotBlank(message = "Alias is required")
    @Size(min=1, max = 20, message = "Alias length must be between 1-20")
    @Column(unique = true, nullable = false)
    private String alias;

    @NotBlank(message = "Gender is required")
    @Pattern(regexp = "male|female", message = "Gender must be in (male, female)")
    @Column(nullable = false, length = 6)
    private String gender;

    @NotBlank(message = "Species is required")
    @Size(min=1, max = 20, message = "Species length must be between 1-20")
    @Column(nullable = false)
    private String species;

    @NotNull(message = "Height is required")
    @Column(nullable = false)
    private Float heightCm;

    @NotNull(message = "Weight is required")
    @Column(nullable = false)
    private Float weightKg;
}
