package com.mgustran.heroes;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import org.springdoc.core.customizers.OperationCustomizer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Arrays;
import java.util.Optional;

@SpringBootApplication
public class HeroesApplication {

	public static void main(String[] args) {
		SpringApplication.run(HeroesApplication.class, args);
	}

	@Bean
	public OpenAPI customOpenAPI() {
		final String securitySchemeName = "bearerAuth";
		return new OpenAPI()
				.components(
						new Components()
								.addSecuritySchemes(securitySchemeName,
										new SecurityScheme()
												.name(securitySchemeName)
												.type(SecurityScheme.Type.HTTP)
												.scheme("bearer")
												.bearerFormat("JWT")
								)
				)
				.info(new Info()
						.title("Heroes API")
						.version("0.1")
						.description("This is a basic documentation for Heroes API")
				).servers(Arrays.asList(
						new Server().url("http://localhost:8080").description("Local server"),
						new Server().url("https://heroapi.mgustran.work").description("Production server")));
	}

	@Bean
	public OperationCustomizer operationCustomizer() {
		return (operation, handlerMethod) -> {
			Optional<PreAuthorize> preAuthorizeAnnotation = Optional.ofNullable(handlerMethod.getMethodAnnotation(PreAuthorize.class));
			StringBuilder sb = new StringBuilder();
			if (preAuthorizeAnnotation.isPresent()) {
				sb.append("This api requires **")
						.append((preAuthorizeAnnotation.get()).value()
								.replaceAll("hasRole|\\(|\\)|'", "")
								.replaceAll(" or ", " | "))
						.append("** permission.");
			} else {
				sb.append("This api is **public**");
			}
			operation.setDescription(sb.toString());
			return operation;
		};
	}

}
