create table HERO
(
    ID        BIGINT auto_increment primary key,
    ALIAS     VARCHAR(20) not null unique,
    GENDER    VARCHAR(6)  not null,
    HEIGHT_CM FLOAT       not null,
    NAME      VARCHAR(20) not null unique,
    SPECIES   VARCHAR(20) not null,
    SURNAMES  VARCHAR(40) not null,
    WEIGHT_KG FLOAT       not null
);
