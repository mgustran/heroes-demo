create table USERS
(
    ID       BIGINT auto_increment primary key,
    EMAIL    VARCHAR(50) unique,
    NAME     VARCHAR(50),
    PASSWORD VARCHAR(100),
    USERNAME VARCHAR(50) unique
);

create table ROLES
(
    ID   BIGINT auto_increment primary key,
    NAME VARCHAR(60) unique
);

create table USER_ROLES
(
    USER_ID BIGINT not null,
    ROLE_ID BIGINT not null,
    primary key (USER_ID, ROLE_ID),
    foreign key (ROLE_ID) references ROLES (ID),
    foreign key (USER_ID) references USERS (ID)
);