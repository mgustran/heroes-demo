#!/bin/bash

docker_image='mgustran/heroapi'
docker_name='heroapi'
it_args=' '
rm_args=' '
b1_args=' '
b2_args=' '
v_args=' '

run_command () {
  echo 'Final command'
  echo "docker run $b1_args --name $docker_name -p 127.0.0.1:8080:8080 $rm_args $docker_image $it_args $b2_args $v_args"
  echo ''
  if ! docker run $b1_args --name $docker_name -p 127.0.0.1:8080:8080 $rm_args $docker_image $it_args $b2_args $v_args; then
    echo "Docker run exit with errors"
  fi
}

interactive_command () {
  echo "docker exec -it $docker_name /bin/sh"
  if ! docker exec -it $docker_name /bin/sh; then
    echo "Interactive mode exit with errors"
  fi
}

echo_help () {
   echo   'Options'
   echo   '    -m -> Maven package'
   echo   '    -b -> Docker build'
   echo   '    -x -> Remove docker previous container before run command'
   echo   '    -r -> Remove docker container after process cancel, use it only with -i'
   printf '    -v -> Add extra args in run command to bind logs and db folders, for external access\n\n'
   echo   'Modes (select only one)'
   echo   '    -i -> Run docker container in background and enter into sh terminal'
   echo   '    -t -> Run docker container in terminal'
   echo   '    -d -> Run docker in background'
   echo   ''
   echo   'Example:   ./docker_util.sh -mbxi    (mvn package, docker build, remove previous container and run interactive mode)'
}

mvn_package () {
  printf "Clean target and build jar\n\n"
  if ! mvn clean package -DskipTests; then
    echo "Maven build exit with errors"
  fi
}

docker_build () {
  printf "\n\n Build docker image \n\n"
  if ! docker build -t $docker_image .; then
    echo "Docker build exit with errors"
  fi
}

remove_container () {
  printf "\n\n Remove %s container \n\n" "$docker_name"
  docker stop $docker_name
  docker rm $docker_name
}

help_true=1

while getopts hmbxrvitd flag
do
    case "${flag}" in
        h)
          echo_help
          exit 0
          ;;
        m)
          mvn_package
          help_true=0
          ;;
        b)
          docker_build
          help_true=0
          ;;
        x)
          remove_container
          help_true=0
          ;;
        r) rm_args=' --rm ';;
        v) v_args=" -v $HOME/heroes-demo/logs:/logs -v $HOME/heroes-demo/db:/db ";;
        i)
          b1_args=' -d '
          b2_args=' --restart unless-stopped '
          run_command
          interactive_command
          exit 0
          ;;
        t)
          run_command
          exit 0
          ;;
        d)
          b1_args=' -d '
          b2_args=' --restart unless-stopped '
          run_command
          exit 0
          ;;

    esac
done

if [ $help_true -eq 1 ]; then
  echo_help
fi
